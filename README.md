# Consignes

1) Faites l'extraction de l'archive ci-jointe dans un serveur web linux apache2 ou nginx avec PHP d'activé.

Assurez-vous que le fichier database.json a bien les droits de lecture et d'écriture pour l'utilisateur www-data.

Cette page permet toutes les secondes de récupérer l'état d'un antivol connecté et de changer son état en cliquant sur les boutons.
Veuillez prendre connaissance des concepts utilisés dans cet exemple (fetch en js, json_decode/json_encode, GET/POST, paramètres json, API, etc).

2) Modifiez le code existant pour récupérer les dernières positions connues de l'antivol et affichez les dans une carte (via la librairie https://leafletjs.com)

3) Faites évoluer le code pour qu'on puisse envoyer au serveur la dernière position gps connue de l'antivol.
