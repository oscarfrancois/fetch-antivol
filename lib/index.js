/**
 * change l'état de l'antivol
 */
function status(statusNew) {
    fetch('api/v1/status.php', {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({ status: statusNew})
    })
}

/**
 * récupération de l'état actuel de l'antivol et mise à jour dans l'interface
 */
function update() {
    fetch('api/v1/status.php')
        .then((response) => response.json())
        .then((enabled) => {
                if (enabled) {
                    message.innerHTML = "Antivol activé"
                    lockStateImg.src = "res/locked.png"
                }
                else {
                    message.innerHTML = "Antivol désactivé"
                    lockStateImg.src = "res/unlocked.png"
                }
                
            }
        );

}
setInterval(update, 1000)