<?php
/**
 * projet: vélosur (antivol connecté)
 * description: modifie si besoin l'état de l'antivol (actif/inactif) et retourne l'état actuel
 * auteur: oscar francois
 * date: 2022-10-19
 * version: 1.0
 */

define("ROOT", ".");

/* Remarque: le stockage est ici effectué dans un fichier pour des raisons de simplicité.
   En conditions réelles, un système multi-utilisateur devrait être mis en place via une base de données.
*/
define("DB_FILENAME", "database.json");

// === si l'on reçoit une requête HTTP de type POST, l'intention est alors de modifier l'état ===
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    // on récupère le contenu de la requête  HTTP
    $request_raw = file_get_contents('php://input');
    // on décode son contenu
    $request = json_decode($request_raw);
    // on modifie l'état de l'antivol
    $db_raw = file_get_contents(ROOT . DIRECTORY_SEPARATOR . DB_FILENAME);
    $db = json_decode($db_raw);
    file_put_contents("/tmp/out.txt", $db->status);
    $db->status = $request->{'status'};
    file_put_contents(ROOT . DIRECTORY_SEPARATOR . DB_FILENAME, json_encode($db));
}

// === récupération de l'état actuel de l'antivol ===

// à transformer en PDO avec connexion à une base de données MariaDB
$db = file_get_contents(ROOT . DIRECTORY_SEPARATOR . DB_FILENAME);
$db = json_decode($db);

// on affiche l'état actuel de l'antivol
$status= $db->status;


// === envoie de l'état actuel de l'antivol an client en json ===

// on indique que le contenu de la réponse n'est pas du HTML mais du json
header("Content-type: application/json");

// envoi du contenu json
echo json_encode($db->status);
?>